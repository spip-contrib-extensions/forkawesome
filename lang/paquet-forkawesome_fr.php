<?php
// Ceci est un fichier langue de SPIP -- This is a SPIP language file


if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// F
	'forkawesome_description' => 'Fork Awesome 5 dans le public et le privé de SPIP',
	'forkawesome_nom' => 'Fork Awesome',
	'fortawesome_slogan' => 'Fork Awesome en police de caractères',
);
