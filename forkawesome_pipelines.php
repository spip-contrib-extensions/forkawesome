<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function forkawesome_header_prive($flux) {
	include_spip('inc/utils');
	$flux .= "\n" . '<link rel="stylesheet" type="text/css" media="all" href="' . find_in_path('fork-awesome/css/fork-awesome.min.css') . '" />';
	return $flux;
}

function forkawesome_insert_head_css($flux) {
	include_spip('inc/utils');
	$flux .= "\n" . '<link rel="stylesheet" type="text/css" media="all" href="' . find_in_path('fork-awesome/css/fork-awesome.min.css') . '" />';
	return $flux;
}

